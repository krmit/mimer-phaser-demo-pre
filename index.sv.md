# Att komam igång med Phaser

Phaser är ett programmeringsbiblotek i huvudsak för att utveckla 2D spel
för webben.

## Komma igång
   * [Start](./start/index.sv.html)
   * [Introduktion till phaser](./introduction/index.sv.html)
   * [Texter](./text/index.sv.html)

## Bilder
   * [Bild](./image/index.sv.html)
   * [Sprite](./sprite/index.sv.html)

## Geometri och Grafik
   * [Grafik](./graphic/index.sv.html)
   * [Geometri](./geometry/index.sv.html)
   * [Färdiga former](./shapes/index.sv.html)

## Input
   * [Händelse](./event/index.sv.html)
   * [Tangentbord](./keyboard/index.sv.html)
   * [Interaktion](./mouse/index.sv.html)

## Simulering
   * [Tweens](./tweens/index.sv.html)
   * [Fysik motor](./physic/index.sv.html)

## Övriga spelobjekt
   * [Fonts](./fonts/index.sv.html)
   * [Partiklar](./particle/index.sv.html)

## Rendering
   * [HTML och CSS](./dom/index.sv.html)
   * [Rendering](./rendering/index.sv.html)

## Strukturen på ett program
   * [Scen](./scene/index.sv.html)

## Utöka phaser
   * [Skapa eget](./customize/index.sv.html)
   * [Plugin](./plugin/index.sv.html)
