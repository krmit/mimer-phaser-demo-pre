"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    this.add.circle(100, 100, 100, 0xff00ff);
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Basic",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
