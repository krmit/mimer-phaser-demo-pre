"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    
    this.add.rectangle(50, 50, 150, 150, 0xff00ff);
    this.add.circle(275, 100, 50, 0xff00ff);
    this.add.ellipse(375, 100, 25, 50, 0xff00ff);
    this.add.line(450, 50, 0, 0, 140, 0, 0xff00ff);
    this.add.triangle(400, 200, 0, 100, 100, 100, 100, 0, 0xff00ff);

  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Primitives",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
