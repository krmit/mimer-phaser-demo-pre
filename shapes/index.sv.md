# Former("shape")

"Shape" är mer komples formerar som redan är färdig implmenterade. 
Implmenteringen utgår från en polygon och är möjlig att interagera med.

## Exempel från andra källor

  * [Phaser IO](http://www.phaser.io/examples/v3/category/game-objects/shapes)

## Kommentarer

## Exempel

### Ett grundläggande exempel

Detta är ett exempel på en enkelel cirkel

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=shapes&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=shapes&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-shapes-basic.html)

### Skapa de grundläggande formerna

Här är exempel på några grundläggande former.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=shapes&example=primitives)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=shapes&example=primitives)
  * ⚙️ [Standalone](./mimer-phaser-standalone-shapes-primitives.html)

### Stjärnform

Här är exempel på en stjärnform.

Läs mer [här](https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.Star.html).

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=shapes&example=star)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=shapes&example=star)
  * ⚙️ [Standalone](./mimer-phaser-standalone-shapes-star.html)
