"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }
  
  preload() {
	  this.load.image('keyboard', config.pathAssets + 'mixed-pictures/keyboard-772x329.png');
  }

  create() {
	  this.image = this.add.image(0, 0, 'keyboard');
	  this.image.scaleX=0.25;
	  this.image.scaleY=0.25;
  }
  
  update() {
	  this.image.x+=0.5;
	  this.image.y+=0.5;
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Move image",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
