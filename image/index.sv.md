# Bilder

Bilder är grunden för alla typer av 2D spel.

Vi utgår här ifrån exemplerna som finns i [labs/game object/image](http://labs.phaser.io/index.html?dir=game%20objects/images/). 

Du kan också hitta andra exempel som kan vara roliga att kombinera med exempel här:
  * [Mesh](http://labs.phaser.io/index.html?dir=game%20objects/mesh/)
  * [Transform](http://labs.phaser.io/index.html?dir=transform/) 

## Repetera
  * Felsökning.

## Nytt
  * Ladda in en bild i minnet och använda det som en resurs genom att använda phaser.
  * Hantera bilder


## Kommentarer

Titta igenom exemplet. Du har fyra funktioner.

## Exempel

### Enklaste

[Enklaste](./00-basic.html) tänkbara exempel.

### Ett grundläggande exemple

Detta är ett exempel för att testa att allt fungerar och du kan se 
dessa exempel så som du vill. 

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=image&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=image&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-image-basic.html)

### Ändra bildens storlek

Att skala om en bild.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=image&example=scale)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=image&example=scale)
  * ⚙️ [Standalone](./mimer-phaser-standalone-image-scale.html)

### Ändra genomskinligheten

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=image&example=alpha)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=image&example=alpha)
  * ⚙️ [Standalone](./mimer-phaser-standalone-image-alpha.html)

### Använd en backgrundsbild
  4. [Använd en backgrundsbild](./03-background.html) tillsammans med andra bilder.

### Rotera en bild
  5. [Rotera en bild](./04-rotation.html).

### Flytta på en bild

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=image&example=move)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=image&example=move)
  * ⚙️ [Standalone](./mimer-phaser-standalone-image-move.html)

### Ladda in flera bilder
  7. [Ladda in flera bilder](./06-spritesheet.html) samtidigt i en så kallad *spritesheet*.

### Anvädn en klass
  8. [Anvädn en klass](./07-class.html) för att enkelt använda en bild många gånger.

### Anvädn en blend
  8. [Anvädn en klass](./07-class.html) för att enkelt använda en bild många gånger.

### Anvädn en tint
  8. [Anvädn en klass](./07-class.html) för att enkelt använda en bild många gånger.

## Uppgifter

Det finns några enkla övningar du kan göra:

  *  [Övningar](./excersise.html)
