"use strict";

let assetsPath = "../../mimer-assets-pre/";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }
  
  preload() {
	  this.load.image('keyboard', assetsPath + 'mixed-pictures/keyboard-772x329.png');
  }

  create() {
	  const image = this.add.image(400, 300, 'keyboard');
  }
}
