"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }
  
  preload() {
	  this.load.image('keyboard', config.pathAssets + 'mixed-pictures/keyboard-772x329.png');
  }

  create() {
	  const image = this.add.image(400, 300, 'keyboard');
	  image.scaleX = 0.5;
	  image.scaleY = 0.5;
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Basic",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
