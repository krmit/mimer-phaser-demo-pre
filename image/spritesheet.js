"use strict";

const assetsPath = "../../mimer-assets-pre/";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }
  
  preload() {
	   this.load.spritesheet('images', assetsPath+'icons/mushrooms-diffrent-color-128x128-7x3.png', { frameWidth: 128, frameHeight: 128 });
  }

  create() {
	  this.add.image(100, 100, 'images',0);
      this.add.image(200, 150, 'images',6);
      this.add.image(300, 200, 'images',7);
      this.add.image(400, 250, 'images',13);
      this.add.image(500, 300, 'images',14);
      this.add.image(600, 350, 'images',20);

  }
}
