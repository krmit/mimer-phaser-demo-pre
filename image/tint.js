"use strict";

let assetsPath = "../../mimer-assets-pre/";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
    this.image={};
  }
  
  preload() {
	  this.load.image('keyboard', assetsPath + 'mixed-pictures/keyboard-772x329.png');
  }

  create() {
	  this.image = this.add.image(400, 300, 'keyboard');
	  this.image.scaleX=0.25;
	  this.image.scaleY=0.25;
  }
  
  update() {
	  this.player.setTint(0x00ff00);
  }
}
