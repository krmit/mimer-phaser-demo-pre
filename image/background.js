"use strict";

let assetsPath = "../../mimer-assets-pre/";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }
  
  preload() {
	  this.load.image('background', assetsPath + 'background/beach-800x600.png');
	  this.load.image('keyboard', assetsPath + 'mixed-pictures/keyboard-772x329.png');
  }

  create() {
	  const background = this.add.image(0, 0, 'background');
	  background.scaleX=2;
	  background.scaleY=2;
	  const keyboard = this.add.image(400, 300, 'keyboard');
	  keyboard.scaleX=0.5;
	  keyboard.scaleY=0.5;
  }
}
