# Bilder

Bilder är grunden för alla typer av 2D spel.

Vi utgår här ifrån exemplerna som finns i [labs/game object/image](http://labs.phaser.io/index.html?dir=game%20objects/images/). 

Du kan också hitta andra exempel som kan vara roliga att kombinera med exempel här:
  * [Mesh](http://labs.phaser.io/index.html?dir=game%20objects/mesh/)
  * [Transform](http://labs.phaser.io/index.html?dir=transform/) 

## Repetera
  * Felsökning.

## Nytt
  * Ladda in en bild i minnet och använda det som en resurs genom att använda phaser.
  * Hantera bilder


## Kommentarer

Titta igenom exemplet. Du har fyra funktioner.

## Exempel

### Enklaste

[Enklaste](./00-basic.html) tänkbara exempel.

### Ändra skalan
  2. [Ändra skalan](./01-scale.html) på en bild.

### Ändra genomskinligheten
  3. [Ändra genomskinligheten](./02-alpha.html) på en bild.

### Använd en backgrundsbild
  4. [Använd en backgrundsbild](./03-background.html) tillsammans med andra bilder.

### Rotera en bild
  5. [Rotera en bild](./04-rotation.html).

### Flytta på en bild
  6. [Flytta på en bild](./05-move.html).

### Ladda in flera bilder
  7. [Ladda in flera bilder](./06-spritesheet.html) samtidigt i en så kallad *spritesheet*.

### Anvädn en klass
  8. [Anvädn en klass](./07-class.html) för att enkelt använda en bild många gånger.

## Uppgifter

Det finns några enkla övningar du kan göra:

  *  [Övningar](./excersise.html)
