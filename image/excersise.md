# Bilder

## Easy
  1. Få en bild att röra sig över skärmen.
  2. Få en bild både röra sig över skärmen och routera.
  3. Se till att du har minst två bilder som rörs sig.
  4. Se till att det se intressant ut!

## Normalt
  1. Ändra alpha på en av bilderna för att få en trevlig effekt.
  2. Använd minst två till effekter på bilderna. 
  3. Se till att ditt demo ser häftigt ut.

## Hard
  1. Se till att din animering förändrar sig över tid. Att det händer något så som att bilden rör sig i ett intressant mönster.

## Very Hard
  1. Se till att ditt demo är häftigt och slår alla med häpnad!

