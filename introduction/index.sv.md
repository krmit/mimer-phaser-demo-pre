# Hur man börjar skriva ett program

## Repetera

  * JSON
  * class

## Nytt

  * scene
  * Skapa ett phaser game objekt.

## Kommentarer

Titta igenom exemplet. Du har tre olika möjligheter att se det.

  * Web - Laddas den direkt från webbservern. Tittar du på detta arkiv lokalt så måste du göra det via en webbserver som du har startat.
  * Editor - En editor där du kan ändra på exemplet. Använder sig av Flems, tyvärr fungerar det inte för alla exempel.
  * Standalone - Liknar webb varianten, du kan anvädna HTML dokumentet direkt på din dator utan några tillägg. Fungerar inte heller för alla exemplen.

### Tips för flems

  * I firefox kan backspace ställa till problem i editorn.
  * -Ända inställning **browser.backspace_action** till 2.
  * -Du hittar inställningen i **about:config** sidan.
  * Stäng av "autoupdate" i flems om du ska koda något längre.

### Tips för standalone

  * Kräver Internet för att kunna startas.
  * Det skulle vara möjligt att skapa en fil som inte kräver det, men den blir väldigt stor.
  
## Exempel

### Ett grundläggande exemple

Detta är ett exempel för att testa att allt fungerar och du kan se 
dessa exempel så som du vill. 

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=introduction&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=introduction&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-introduction-basic.html)

### Ett ändra på bagrundfärgen

### Ändra storleken i config

### Ändra create i config

### Ändra preload i config

### Ändra update i config
  
## Övningar

### Exempel

Det finns några enkla övningar du kan göra. Hät är ett exempel.

  *  [Övningar](./excersise.html)
