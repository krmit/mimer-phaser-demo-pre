"use strict";

// Path assets in gitlab project mimer-assets-pre
let assetsPath = "../../mimer-assets-pre/";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }

  preload() {
    this.load.spritesheet('particels', assetsPath+'particel/many-colors-400x400-9x1.png', { frameWidth: 400, frameHeight: 400 });
  }

  create() {
    const particles = this.add.particles("particels");

    particles.createEmitter({
      frame: { frames: [0,1,2,3,4,5,6,7,8], cycle: true },
      x: 64,
      y: { min: 500, max: 100, steps: 32 },
      lifespan: 4000,
      accelerationX: 400,
      scale: 0.1,
      frequency: 50
    });
  }
}
