# Att komma igång med Phaser

Phaser är ett programmeringsbiblotek i huvudsak för att utveckla 2D spel
för webben.

Du behöver en webbserver för att utveckla phaser på ett enkelt sätt. 

## Repetera

  * Webbserver
  * Git
  * HTML

## Nytt.

  * Installera webbserver.

## Exempel

Här är ett par exempel på vad phaser kan göra.

  * ⚙️ [Partikel](../tools/mimer-phaser-webserver.html?topic=start&example=particel) är ett exempel som visar på flera olika funktioner hos phaser. 
  * 🔗 [Phaser Examples](http://www.phaser.io/examples)
  
## Alternativ

För att kunna anvnäda dessa exempel finns det flera alternativ.

### Flems

Funderar inte för alla exemplerna

### Standalone

Detta kräver någon form av rendering script som jag inte skrivit klart ännu ... Om det alls är möjligt.

### En lokal webbserver

#### Klona demo och resurser arkiven.

Du behöver ha två arkiv som ska ligga i samma mapp. Förslagsviss skapar 
du en ny map web för detta.

  * Skapa en ny mapp och gå till den. **mkdir web; cd web**
  * Klona resurserna för projektet. **git clone https://gitlab.com/krmit/mimer-assets-pre.git**
  * Klona exemplerna. **git clone https://gitlab.com/krmit/mimer-phaser-demo-pre.git** 

### Installera webbserver

Vi rekomenderar att du använder de enkla webbserveran [ran](https://github.com/m3ng9i/ran) eller 
(http-server)[https://www.npmjs.com/package/http-server]. Men du skulle 
också kunna använda någon annan server om du vill. Det väst

#### ran

Borde fungera på alla system som du får starta ett program på.

  * Gå till mappen som har arkiven ovan.
  * [ran:s nedladning](https://github.com/m3ng9i/ran/releases) - Ladda en binär för ditt OS från 
  * Döp binären till **ran**
  * **ran -l -p=8080**, där 8080 är porten som du vill ha din webbserver på.
  * [http://127.0.0.1:8080](http://127.0.0.1:8080) - Öppna följande länk i webbläsaren 
  

#### http-server

Kräver att du först har installerat node och npm på ditt system.

  * Installera http-server: **sudo npm -g install http-server**
  * Gå till mappen som har arkiven ovan.
  * **http-server -p 8080**, där 8080m är porten som du vill ha din webbserver på.
  * Öppna följande länk i webbläsaren [http://127.0.0.1:8080](http://127.0.0.1:8080)

**Tips!** Detta fungera på valhallserver välj port X och du kommer hitta din server på URL.
