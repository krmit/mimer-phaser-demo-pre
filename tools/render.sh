#!/bin/bash

find * -type d  \! -path \*/\.git\* \! -path \*tools\* -exec bash -c "echo -e '\e[32m{}'; cd {};pwd;ghmd *.md;ghmd *.sv.md;" \;
echo "Refactor";
ghmd index.sv.md;
mv index.sv.html index.html
echo "Uppload to server.";
scp -r */ index.html htsit.se:~/www/b/
