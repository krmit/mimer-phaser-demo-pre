# Vanlig Text

## Easy
Användbara style egenskaper för text är: align, color, fontSize, fontFamily, backgroundColor, fontStyle. 

  * Fyll variablerna  textOne, textTwo och textThree med en text på två till fyra ord. Fyll sedan variabeln textFour med en längre text som innehåller många rader.
  * Implementera funktionen testStyleOne. I funktionen använd kedjade metoder. Du ska använda minst två olika styles. Du ska returnera en text med en intressant stil.
  * Implementera funktionen testStyleTwo. I funktionen använd JSON för att konfigurera texten. Du ska använda minst två olika styles. Du ska returnera en text med en intressant stil.
  * Implementera funktion testStyleThree som ovan på det sätt som du tycker bäst om. Men se till att du använder alla olika typer av style. 

## Easy Extra

  * Implementera funktion testStyleFour som ovan på det sätt som du tycker bäst om. Men se till att stilen är väsentligt annorlunda än vad du gjort tidigare, spännande och snygg.

## Normal

  * Med hjälp av “padding” har vi något som liknar en enklare Boxmodell i Phaser. Använd funktionen testStyleFive och variabeln testFive. Sätt ett bakgrundsfärg och “padding” så vi kan se alla de olika marginaler kring texten som man kan sätta.

## Normal Extra

  * Få texten som returneras ifrån testStyleFive  att rotera. Du får använda egenskapen route som alla texter har och lägga till något i update metoder.

## Hard

  * Få texten som returneras av testStyleSix att  kontinuerligt byter färg. Du kan göra något i updat method.
