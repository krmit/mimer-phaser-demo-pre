# Vanlig Text

Utgå ifrån dessa exempel: (labar)[http://labs.phaser.io/index.html?dir=game%20objects/text/static/] 

## Repetera
  * Basic types
  * JSON
  * Funktion/Metoder

## Nytt
   * [Kedja av metoder](https://schier.co/blog/2013/11/14/method-chaining-in-javascript.html);
   * phaser
   * Game object text.

## Kommentarer

Du kan använda text för att visa phaser texter i ditt program.

## Exempel

  * [Enkelaste](../tools/mimer-phaser-webserver.html?topic=text&example=basic) tänkbara exempel.
  * [Med enkel stil]((../tools/mimer-phaser-webserver.html?topic=text&example=simpel-style.html) och konfigurat med ett object.

### Enklaste


### Med Stil

### Padding

### Flera radier i en text.

### Aline

### Skuggor

### Webbfonts

## Uppgifter 

Det finns några enkla övningar du kan göra:

  *  [Övningar](./excersise.html)
