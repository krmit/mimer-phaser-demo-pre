# Händelser

## Repetera
  * JSON
  * class

## Nytt
  * scene
  * Skapa ett phaser game objekt.

## Kommentarer

Titta igenom exemplet. Du har fyra funktioner.

## Exempel

  * [Enklaste](./00-basic.html) tänkbara exempel. 

## Uppgifter

Det finns några enkla övningar du kan göra:

  *  [Övningar](./excersise.html)
