# Partiklar

## Exempel från andra källor

  * [Phaser IO - Particle emitter](http://www.phaser.io/examples/v3/category/game-objects/particle-emitter)

## Kommentarer

Att emittera partiklar, vilket kan skapa många roliga effekter.

## Exempel

### Ett grundläggande exemple

Detta är ett exempel på att visa upp en enkel effekt.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=particle&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=particle&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-particle-basic.html)
