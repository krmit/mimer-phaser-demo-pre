# Alternativa fonter

## Exempel från andra källor

  * [Phaser IO - Static text](http://www.phaser.io/examples/v3/category/game-objects/bitmaptext/static)
  * [Phaser IO - Dynamic effects](http://www.phaser.io/examples/v3/category/game-objects/bitmaptext/dynamic)

## Kommentarer

Att vilken font du använder kan ha stor betydelse för hur ditt spel uppfatas.

## Exempel

### Ett grundläggande exemple

Detta är ett exempel på att skriva något väldigt enkelt.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=font&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=font&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-font-basic.html)
