"use strict";

class Ship extends Phaser.GameObjects.Sprite {

    constructor (scene, x, y)
    {
        super(scene, x, y);

        this.setTexture('ship');
        this.setPosition(x, y);
    }

    preUpdate (time, delta)
    {
        super.preUpdate(time, delta);

        this.rotation += 0.01;
    }

}

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }
  
  preload ()
  {
      this.load.image('ship', config.pathAssets + 'ships/nightraider-224x154.png');
  }

  create() {
	this.add.existing(new Ship(this, 100, 100));
    this.add.existing(new Ship(this, 200, 300));
    this.add.existing(new Ship(this, 300, 500));
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Class",
    width: 800,
    height: 600,
    backgroundColor: "#fff",
    parent: "phaser-demo",
    scene: [GameScene]
};
