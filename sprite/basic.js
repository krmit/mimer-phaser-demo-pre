"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }

  preload ()
  {
      this.load.image('ship', config.pathAssets + 'ships/nightraider-224x154.png');
  }
  
  create() {
	this.add.sprite(200, 200, 'ship');
  }

}

export const config = {
    type: Phaser.WEBGL,
    name: "Basic",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
