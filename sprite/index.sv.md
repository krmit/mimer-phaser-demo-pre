# Sprite

Är ett extra sprite 

Vi utgår här ifrån exemplerna som finns i [labs/game object/sprite](https://labs.phaser.io/index.html?dir=game%20objects/sprites/&q=). 

## Repetera
  * image

## Nytt
  * sprite

## Kommentarer

Sprite är en bild objekt med extra egenskaper. Skillnaden är ofta liten.

## Exempel

### Enklaste

Enklaste tänkbara sprite.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=sprite&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=sprite&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-sprite-basic.html)

### Class

Vi kan definera uppgifter med en klass.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=sprite&example=class)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=sprite&example=class)
  * ⚙️ [Standalone](./mimer-phaser-standalone-sprite-class.html)

## Uppgifter

Det finns några enkla övningar du kan göra:

  *  [Övningar](./excersise.html)
