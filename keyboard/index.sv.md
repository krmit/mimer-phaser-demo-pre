# Tangentbord

## Repetera
  * JSON
  * class

## Nytt
  * scene
  * Skapa ett phaser game objekt.

## Kommentarer

Titta igenom exemplet. Du har fyra funktioner.

## Exempel

### Enklaste

Enklaste tänkbara keyboard.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=keyboard&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=keyboard&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-keyboard-basic.html)

### Enklaste

Enklaste tänkbara keyboard.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=keyboard&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=keyboard&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-keyboard-basic.html)

## Uppgifter

Det finns några enkla övningar du kan göra:

  *  [Övningar](./excersise.html)
