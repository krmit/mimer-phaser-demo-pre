"use strict";

// Functions that you can use
export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: true });
  }

  create() {
    const title = this.add.text(100, 100, "HTSIT har \npengar!", {
      fontFamily: "Helvetica",
      fontSize: 96,
      color: "#00ff00"
    });
    title.text = "HTSIT har \nframtidstro!";
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Basic keyboard",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
