"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    const graphics = this.add.graphics();
    graphics.fillStyle(0x0000ff);
    graphics.fillRect(50, 50, 150, 150);
    graphics.fillCircle(275, 100, 50);
    graphics.fillEllipse(375, 100, 25,50);
    graphics.fillPoint(475, 100, 10);
    graphics.fillRoundedRect(550, 50, 150, 150, 20);
    
    graphics.fillTriangle(50, 250, 50, 350, 150,250)
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Primitives",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
