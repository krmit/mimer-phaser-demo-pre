# Grafik

Grafik klassen gör det möjligt att rita med hjälpa av phaser.

## Exempel från andra källor

  * [Phaser IO](http://www.phaser.io/examples/v3/category/game-objects/graphics)

## Kommentarer

Att rita något i ett spel är ofta kritisk för prestandan på spelet. 
Dessa grafik kommandon skickas i en "batch" till vårt back-end för 
grafik. Ofta någon drivrutin för grafikaccelerering. Phaser 
automatisera denna process till stor del.

## Exempel

### Ett grundläggande exemple

Detta är ett exempel på att rita något väldigt enekelt.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-image-graphic.html)

### Rita en grundläggande form

Här ritas många olika former.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=primitives)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=primitives)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-primitives.html)

### Fyll en grundläggande form

Här fyller vi en grundläggande form

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=primitives-fill)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=primitives-fill)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-primitives--fill.html)

### Rita en linje

Här rittas en linje med hjälp av path

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=path)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=path)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-path.html)

### Fyll i ett område utifrån en linje

Här rittas ett område med hjälp av en linje och sedan fylls innehållet.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=path-fill)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=path-fill)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-path--fill.html)

### Fyll och rita i ett område utifrån en linje

Kombinerar de två exemplena ovan.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=path-both)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=path-both)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-path--both.html)

### Rita i update funktionen

Ska en enkel animering.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=update)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=update)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-update.html)

### Animera med hjälp av grafik objektet

Ska en enkel men bra animering med hjälp v clear.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=update-clear)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=update-clear)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-update--clear.html)

### Animera med hjälp av grafik objektet på ett bättre sätt

Ska en enkel men bra animering genom att ändra på egenskaper.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=graphic&example=update-xy)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=graphic&example=update-xy)
  * ⚙️ [Standalone](./mimer-phaser-standalone-graphic-update--xy.html)
