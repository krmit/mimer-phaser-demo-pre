"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    const graphics = this.add.graphics();
    graphics.lineStyle(10, 0xff0000);
    graphics.strokeRect(50, 50, 150, 150);
    graphics.strokeCircle(275, 100, 50);
    graphics.strokeEllipse(375, 100, 25,50);
    graphics.strokeRoundedRect(550, 50, 150, 150, 20);
    
    graphics.strokeTriangle(50, 250, 50, 350, 150,250)
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Primitives",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
