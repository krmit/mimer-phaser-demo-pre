"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
    this.delta = 0;
    this.graphics;
  }
  
  create() {
    this.graphics = this.add.graphics();
    this.graphics.fillStyle(0x0000ff);
    this.graphics.lineStyle(10, 0x00ff00);
  }

  update() {
    this.delta += 1; 

    this.graphics.beginPath();
    this.graphics.moveTo(this.delta+100, this.delta+100);
    this.graphics.lineTo(this.delta+200, this.delta+100);
    this.graphics.lineTo(this.delta+200, this.delta+200);
    this.graphics.lineTo(this.delta+100, this.delta+200);
    this.graphics.closePath();
    this.graphics.fillPath();
    this.graphics.strokePath();
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Path in update",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
