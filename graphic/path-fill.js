"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    const graphics = this.add.graphics();
    graphics.fillStyle(0x0000ff);
    graphics.beginPath();
    graphics.moveTo(100, 100);
    graphics.lineTo(200, 200);
    graphics.lineTo(200, 100);
    graphics.closePath();
    graphics.fillPath();
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Fill path",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
