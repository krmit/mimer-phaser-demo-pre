"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    const graphics = this.add.graphics();
    graphics.fillStyle(0x0000ff);
    graphics.fillRect(50, 50, 150, 150);
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Basic",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
