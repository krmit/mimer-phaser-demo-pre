"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    const graphics = this.add.graphics();
    graphics.lineStyle(10, 0xffff00);
    graphics.fillStyle(0xffff00);
    
    const rect = new Phaser.Geom.Rectangle(50, 50, 150, 150);
    graphics.strokeRectShape(rect);
    
    const circle = new Phaser.Geom.Circle(275, 100, 50);
    graphics.strokeCircleShape(circle);
    
    const ellipse = new Phaser.Geom.Ellipse(375, 100, 25, 50);
    graphics.strokeEllipseShape(ellipse);
    
    const line = new Phaser.Geom.Line(450, 50, 550, 150);
    graphics.strokeEllipseShape(line);
    
    const triangel = new Phaser.Geom.Triangle(50, 250, 50, 350, 150,250);
    graphics.strokeTriangleShape(triangel);
    
    const point = new Phaser.Geom.Point(200, 300);
    graphics.fillPointShape(point, 20)
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Primitives",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
