"use strict";

export class GameScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "GameScene", active: true });
  }

  create() {
    const circle =  new Phaser.Geom.Circle(250, 250, 250);
    const graphics = this.add.graphics();
    graphics.fillStyle(0x00ff00);
    graphics.fillCircleShape(circle);
  }
}

export const config = {
    type: Phaser.WEBGL,
    name: "Basic",
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [GameScene]
};
