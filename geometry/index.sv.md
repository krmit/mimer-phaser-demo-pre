# Geometri

Geometri används för att representera geometriska former i spelet. 
Dessa behvöer inte nödvändiviss ritas ut. Till exempel "hitbox". 

## Exempel från andra källor

  * [Phaser IO](http://www.phaser.io/examples/v3/category/geom)

## Kommentarer

För att vi ska kunna se dem behöver vi rita ut dem. Det finns 
funktioner i grafik objektet för att göra det.

## Exempel

### Ett grundläggande exempel

Detta är ett exempel på en enkelt geometri.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=geometry&example=basic)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=geometry&example=basic)
  * ⚙️ [Standalone](./mimer-phaser-standalone-geometry-basic.html)

### Skapa de grundläggande formerna

Här är exempel på några grundläggande geometriska former. 
Använder "stroke2 från "graphic" för att skriva ut dem.

  * ⚙️ [Web](../tools/mimer-phaser-webserver.html?topic=geometry&example=primitives)
  * ⚙️ [Editor](../tools/mimer-phaser-flems.html?topic=geometry&example=primitives)
  * ⚙️ [Standalone](./mimer-phaser-standalone-geometry-primitives.html)

